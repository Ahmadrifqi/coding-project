There is two folders:
1. Computational Aerodynamics (Bachelor)
2. Computational Fluid Dynamics (Master)

Overall, the project is similiar with a slight difference in Computational Fluid Dynamics. There is meshing sub-routine for airfoil.
Requirements :
1. C++ (11 or newer, likely)
2. GnuPlot (for Plotting, newest)
3. GnuPlot User Guide PDF (if you want to change any of the plot code, http://www.gnuplot.info/docs_5.0/gnuplot.pdf)
4. Xming (for windows user only, https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/ )
5. Patience