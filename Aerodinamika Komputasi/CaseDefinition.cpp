#include <vector>
#include <iostream>
using namespace std;

vector<vector<double> > CaseDefinition(int meshx, int meshy, double NorthBC, double SouthBC, double WestBC, double EastBC){
	vector<vector<double> > Case(meshy+2);

	for (int i=0; i<meshy+2; i++){
		Case[i] = vector<double>(meshx+2);
		for (int j=0; j<meshx+2; j++){
			Case[i][j] = 0;
		}
	}

	for (int i=0; i<meshy+2; i++){
		for (int j=0; j<meshx+2; j++){
			if (i == 0 && j > 0 && j < meshx+1){
				Case[i][j] = SouthBC;
			}
			if (i == meshy+1 && j > 0 && j < meshx+1){
				Case[i][j] = NorthBC;
			}
			if (j == 0 && i > 0 && i < meshy+1){
				Case[i][j] = WestBC;
			}
			if (j == meshx+1 && i > 0 && i < meshy+1){
				Case[i][j] = EastBC;
			}
		}
	}
	std::cout << "Case Definiton ends" << std::endl;
	
	return Case;
}
