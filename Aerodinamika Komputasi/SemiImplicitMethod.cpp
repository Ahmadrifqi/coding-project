/*
This Method was created under the assumption that the domain is rectangular/square
*/
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

#include "cartesian.cpp"
#include "LinearEquationSolver.h"
using namespace std;

vector<vector<double> > SemiImplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate){
	//variables definition
	cout<<"Perhitungan Metode Semi-Implisit Dimulai!!"<<endl;
	vector<vector<double> > UpdatedCase(Case.size());
	vector<vector<double> > OldCase(Case.size());
	vector<vector<double> > matrix(Case[0].size()-2);
	vector<double> bvec(Case[0].size()-2);
	vector<double> temp;
	double relaxation_param = 0.8;
	double error = 0;
	double dx = 0;
	double dy = 0;
	int size = 0;

	for (int i=0; i<Case.size(); i++){
		UpdatedCase[i] = vector<double>(Case[i].size());
		OldCase[i] = vector<double>(Case[i].size());
		for (int j=0; j<Case[i].size(); j++){
			UpdatedCase[i][j] = Case[i][j];
			OldCase[i][j] = Case[i][j];
		}
	}

	for (int i=0; i<Case.size()-2; i++){
		matrix[i] = vector<double>(Case[i].size()-2);
		bvec[i] = 0;
		for (int j=0; j<Case[i].size()-2; j++){
			matrix[i][j] = 0;
		}
	}

	do{
		error = 0;
		for (int i=1; i<Case.size()-1; i++){
			for (int j=1; j<Case[i].size()-1; j++){
				dx = Coordinate[i][j+1].x - Coordinate[i][j].x;
				dy = Coordinate[i+1][j].y - Coordinate[i][j].y;

				matrix[j-1][j-1] += (dx/dy) + (dx/dy);

				bvec[j-1] += OldCase[i-1][j] * dx/dy + OldCase[i+1][j] * dx/dy;

				if (Case[i][j-1] != 0 && j < Case[i].size()-2){
					bvec[j-1] += OldCase[i][j-1] * dy/dx;

					matrix[j-1][j-1] += (dy/dx);
					matrix[j-1][j-1+1] -= (dy/dx);
				}

				if (Case[i][j+1] != 0 && j > 1){
					bvec[j-1] += OldCase[i][j+1] * dy/dx;

					matrix[j-1][j-1] += (dy/dx);
					matrix[j-1][j-1-1] -= (dy/dx);
				}
			}
			temp = TDMA(matrix, bvec);
			for (int j=0; j<Case.size()-2; j++){
				UpdatedCase[i][j+1] = temp[j];
			}
		}

		for (int i=0; i<matrix.size(); i++){
			bvec[i] = 0;
			for (int j=0; j<matrix[i].size(); j++){
				matrix[i][j] = 0;
			}
		}

		for (int i=0; i<OldCase.size(); i++){
			for (int j=0; j<OldCase[i].size(); j++){
				double cek = std::abs(UpdatedCase[i][j] - OldCase[i][j]);
				error = std::max(error, cek);
				OldCase[i][j] = UpdatedCase[i][j];
			}
		}
		cout<<"Error perhitungan adalah : "<<error<<endl;
	}
	while (error > 0.0001);

	return UpdatedCase;
}