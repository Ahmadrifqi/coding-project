/*
	The Domain is Assumed to have constannt dy and dx
*/
#include <vector>
#include <cmath>

using namespace std;

vector<vector<cartesian> > Mesh(double Lengthx, double Lengthy, int meshx, int meshy){
	vector<vector<cartesian> > Results(meshy+2);
	double basex = 0.0;
	double basey = 0.0;
	double dx = Lengthx/meshx;
	double dy = Lengthy/meshy;

	for (int i=0; i<meshy+2; i++){
		Results[i] = vector<cartesian>(meshx+2);
	}

	Results[0][0].y = basey - dy/2.0;
	Results[0][0].x = basex - dx/2.0;
	Results[0][meshx+1].y = basey - dy/2.0;
	Results[0][meshx+1].x = Lengthx + dx/2.0;
	Results[meshy+1][0].y = Lengthy + dy/2.0;
	Results[meshy+1][0].x = basex - dx/2.0;
	Results[meshy+1][meshx+1].y = Lengthy + dy/2.0;
	Results[meshy+1][meshx+1].x = Lengthx + dx/2.0;

	for (int i=1; i<meshy+1; i++){
		Results[i][0].y = Results[i-1][0].y + dy;
		Results[i][meshx+1].y = Results[i-1][meshx+1].y + dy;

		Results[i][0].x = Results[i-1][0].x;
		Results[i][meshx+1].x = Results[i-1][meshx+1].x;
	}

	for (int i=1; i<meshx+1; i++){
		Results[0][i].x = Results[0][i-1].x + dx;
		Results[meshy+1][i].x = Results[meshy+1][i-1].x + dx;

		Results[0][i].y = Results[0][i-1].y;
		Results[meshy+1][i].y = Results[meshy+1][i-1].y;
	}

	for (int i=1; i<meshy+1; i++){
		for (int j=1; j<meshx+1; j++){
			Results[i][j].x = Results[i][j-1].x + dx;
			Results[i][j].y = Results[i-1][j].y + dy;
		}
	}

	return Results;	
}
	