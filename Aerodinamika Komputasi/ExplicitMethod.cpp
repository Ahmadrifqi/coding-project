#include <cmath>
#include <vector>
#include <iostream>
#include "Method.h"
using namespace std;

vector<vector<double> > ExplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate){
	//variables definition
	cout<<"Perhitungan Metode Eksplisit Dimulai!!"<<endl;
	vector<vector<double> > UpdatedCase(Case.size());
	vector<vector<double> > OldCase(Case.size());
	double relaxation_param = 0.8;
	double error = 0;
	double dx = 0;
	double dy = 0;

	for (int i=0; i<Case.size(); i++){
		UpdatedCase[i] = vector<double>(Case[i].size());
		OldCase[i] = vector<double>(Case[i].size());
		for (int j=0; j<Case[i].size(); j++){
			UpdatedCase[i][j] = Case[i][j];
			OldCase[i][j] = Case[i][j];
		}
	}

	do{
		error = 0;
		for (int i=1; i<Case.size()-1; i++){
			for (int j=1; j<Case[i].size()-1; j++){
				double Temp = 0;
				dx = Coordinate[i][j+1].x - Coordinate[i][j].x;
				dy = Coordinate[i+1][j].y - Coordinate[i][j].y;
				Temp = UpdatedCase[i+1][j]/dy + UpdatedCase[i-1][j]/dy + UpdatedCase[i][j+1]/dx + UpdatedCase[i][j-1]/dx;
				UpdatedCase[i][j] = OldCase[i][j] + relaxation_param * (-OldCase[i][j] + Temp/(2.0/dx + 2.0/dy));
			}
		}


		for (int i=1; i<Case.size()-1; i++){
			for (int j=1; j<Case[i].size()-1; j++){
				double Temp = std::abs(UpdatedCase[i][j] - OldCase[i][j]);
				error = std::max(Temp, error);
				OldCase[i][j] = UpdatedCase[i][j];
			}
		}
		cout<<"Error perhitungan adalah : "<<error<<endl;
	}
	while (error > 0.0001);

	return UpdatedCase;
}