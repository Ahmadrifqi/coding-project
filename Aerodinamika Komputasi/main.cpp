#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "cartesian.cpp"
#include "Mesh.cpp"
#include "CaseDefinition.cpp"
#include "Method.h"
using namespace std;

int main(){
	ofstream OutMeshing;
	ofstream OutHasil;
	OutMeshing.open("Meshing.txt");
	OutHasil.open("Hasil.txt");

	int x = 5;
	int y = 4;
	double Lengthx = 1;
	double Lengthy = 1;
	double NorthBC = 0.1;
	double SouthBC = 0.1;
	double WestBC = 0.1;
	double EastBC = 0.1;

	vector<vector<cartesian> > ScalarPoint = Mesh(Lengthx, Lengthy, x, y);
	OutMeshing<<"# Meshing.txt"<<endl;
	OutMeshing<<"# X"<<"\t"<<"Y"<<endl;
	for (int i=0; i<ScalarPoint.size(); i++){
		for (int j=0; j<ScalarPoint[i].size(); j++){
			OutMeshing<<ScalarPoint[i][j].x<<"\t"<<ScalarPoint[i][j].y<<endl;
		}
	}
	OutMeshing.close();

	vector<vector<double> > Case = CaseDefinition(x, y, NorthBC, SouthBC, WestBC, EastBC);

	vector<vector<double> > results = SemiImplicitMethod(Case, ScalarPoint);

	vector<vector<double> > eksplisit = ExplicitMethod(Case, ScalarPoint);

	OutHasil<<"# Meshing.txt"<<endl;
	OutHasil<<"# X"<<"\t"<<"Y"<<"\t"<<"Z"<<endl;
	for (int i=0; i<ScalarPoint.size(); i++){
		for (int j=0; j<ScalarPoint[i].size(); j++){
			OutHasil/*<<ScalarPoint[i][j].x<<"\t"<<ScalarPoint[i][j].y<<"\t"*/<<eksplisit[i][j]<<"\t";//<<endl;
		}
		OutHasil<<endl;
	}
	OutHasil.close();

	//vector<vector<double> > SI = SemiImplicitMethod(Case, ScalarPoint);

	return 0;
}