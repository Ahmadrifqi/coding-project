#include <vector>
using namespace std;

#ifndef _LINEAREQUATIONSOLVER_H_
#define _LINEAREQUATIONSOLVER_H_

vector<double> GaussJordan(vector<vector<double> > &Matrix, vector<double> &Vector);
vector<double> GaussSeidel(vector<vector<double> > &Matrix, vector<double> &Vector);
vector<double> TDMA(vector<vector<double> > &Matrix, vector<double> &Vector);
vector<double> MultiGrid(vector<vector<double> > &Matrix, vector<double> &Vector);

#endif
