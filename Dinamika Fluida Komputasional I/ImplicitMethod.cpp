#include <iostream>
#include <cmath>
#include <vector>

#include "LinearEquationSolver.h"
#include "Method.h"
#include "cartesian.cpp"
using namespace std;

vector<vector<double> > ImplicitMethod(vector<vector<double> > &Case, vector<vector<cartesian> > &Coordinate){
	//variables definition
	cout<<"Perhitungan Metode Implisit Dimulai!!"<<endl;
	vector<vector<double> > UpdatedCase(Case.size());
	int dim = (Case.size()-2) * (Case[0].size()-2);
	vector<vector<double> > matrix(dim);
	vector<double> bvec(dim);
	vector<double> Temp(dim);
	double dx = 0;
	double dy = 0;
	int num = 0;

	for (int i=0; i<dim; i++){
		matrix[i] = vector<double>(dim);
		for (int j=0; j<dim; j++){
			matrix[i][j] = 0;
		}
		bvec[i] = 0;
	}

	for (int i=0; i<Case.size(); i++){
		UpdatedCase[i] = vector<double>(Case[i].size());
		for (int j=0; j<Case[i].size(); j++)
			UpdatedCase[i][j] = Case[i][j];
	}

	for (int i=1; i<Case.size()-1; i++){
		for (int j=1; j<Case[i].size()-1; j++){
			if (i != Case.size()-2)
				dy = Coordinate[i-1+1][j-1].y - Coordinate[i-1][j-1].y;
				
			if (j != Case[i].size()-2)
				dx = Coordinate[i-1][j-1+1].x - Coordinate[i-1][j-1].x;
			
			matrix[num][num] += 2*(dy/dx) + 2*(dx/dy);
			
			if (Case[i][j-1] != 0)
				bvec[num] += Case[i][j-1] * dy/dx;

			else
				matrix[num][num-1] -= dy/dx;

			if (Case[i][j+1] != 0)
				bvec[num] += Case[i][j+1] * dy/dx;

			else
				matrix[num][num+1] -= dy/dx;

			if (Case[i-1][j] != 0)
				bvec[num] += Case[i-1][j] * dx/dy;

			else
				matrix[num][num-(Case.size()-2)] -= dx/dy;

			if (Case[i+1][j] != 0)
				bvec[num] += Case[i+1][j] * dx/dy;

			else
				matrix[num][num+(Case.size()-2)] -= dx/dy;

			if (j > (Case[i].size()/3) && j < (Case[i].size()*2/3) && i == (Case.size()/2-1) && i < Case.size()-2)
				matrix[num][num+(Case.size()-2)] += dx/dy;

			else if (j > (Case[i].size()/3) && j < (Case[i].size()*2/3) && i == (Case.size()/2) && i > 2)
				matrix[num][num-(Case.size()-2)] += dx/dy;
		
			num++;
		}
	}

	Temp = GaussSeidel(matrix, bvec);
	num = 0;
	for (int i=1; i<Case.size()-1; i++){
		for (int j=1; j<Case[i].size()-1; j++){
			UpdatedCase[i][j] = Temp[num];
			num++;
		}
	}

	//Delete unnecessary vector
	bvec.erase(bvec.begin(), bvec.begin()+bvec.size());
	Temp.erase(Temp.begin(), Temp.begin()+Temp.size());
	matrix.erase(matrix.begin(), matrix.begin()+matrix.size());
	return UpdatedCase;
}
