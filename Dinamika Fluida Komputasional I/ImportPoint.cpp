#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "cartesian.cpp"
using namespace std;

vector<cartesian> ImportPoint(string namefile){
	vector<double> x;
	vector<double> y;
	double input = 0;
	int counter = 0;
	cout<<namefile<<endl;
	const char* inputfile = namefile.c_str();
	fstream MyFile(inputfile, ios::in);

	if (!MyFile.is_open()) {
        std::cerr << "There was a problem opening the input file!\n";
    }

	while (MyFile >> input){
		if (counter % 2 == 0){
			x.push_back(input);
		}
		else{
			y.push_back(input);
		}
		counter++;
	}
	MyFile.close();

	vector<cartesian>  ReadFile(x.size());

	for (int i=0; i<x.size(); i++){
		ReadFile[i].x = x[i];
		ReadFile[i].y = y[i];
	}

	//Delete unnecessary vector
	x.erase(x.begin(), x.begin()+x.size());
	y.erase(y.begin(), y.begin()+y.size());

	return ReadFile;
}
