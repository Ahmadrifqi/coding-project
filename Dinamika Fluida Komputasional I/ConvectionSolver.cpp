#include <iostream>
#include <vector>
#include <cmath>

#include "cartesian.cpp"
#include "LinearEquationSolver.h"
using namespace std;

vector<vector<double> > ConvectionSolver(vector<vector<cartesian> > &Coordinate, vector<vector<double> > &Case, 
	vector<vector<double> > &Casemindt, vector<vector<double> > &Casemin2dt, float dt, float totaltime){
	int dim = Case[0].size()-2;
	vector<vector<double> > UpdatedCase(Case.size());
	vector<vector<double> > OldCase(Case.size());
	vector<vector<cartesian> > velocity(Case.size());
	vector<vector<double> > matrix(Case[0].size()-2);
	vector<double> bvec(Case[0].size()-2);
	vector<double> temp(Case[0].size()-2);
	double error, dy, dx;

	for (int i=0; i<Case[0].size()-2; i++){
		matrix[i] = vector<double>(Case[0].size()-2);
		bvec[i] = 0;
		for (int j=0; j<Case[0].size()-2; j++){
			matrix[i][j] = 0;
		}
	}

	for (int i=0; i<Case.size(); i++){
		velocity[i] = vector<cartesian>(Case[i].size());
		UpdatedCase[i] = vector<double>(Case[i].size());
		OldCase[i] = vector<double>(Case[i].size());
		for (int j=0; j<Case[i].size(); j++){
			velocity[i][j].x = 10.0;
			velocity[i][j].y = 0;
			OldCase[i][j] = Case[i][j];
			UpdatedCase[i][j] = Case[i][j];
		}
	}

	do{
		error = 0;
		for (int i=1; i<Case.size()-1; i++){
			for (int j=1; j<Case[i].size()-1; j++){
				int J = j-1;
				if (i != Case.size()-2)
					dy = Coordinate[i][J].y - Coordinate[i-1][J].y;
				
				if (j != dim)
					dx = Coordinate[i-1][J+1].x - Coordinate[i-1][J].x;

				if (totaltime == dt){ //transient term using first order implicit
					matrix[J][J] += 1/dt;

					bvec[J] += Casemindt[i][j]/dt;
				}

				else { //transient term with SOUE 
					matrix[J][J] += 3.0/(2.0*dt);

					bvec[J] += 2.0/dt * Casemindt[i][j] - Casemin2dt[i][j]/(2.0*dt);
				}

				//north neighbour 
				matrix[J][J] += std::max(0.0, velocity[i][j].y) * dx;
				bvec[J] -= Casemindt[i][j] * std::min(0.0, velocity[i][j].y) * dx;

				//south neighbour
				matrix[J][J] -= std::min(0.0, velocity[i][j].y) * dx;
				bvec[J] += Casemindt[i][j] * std::max(0.0, velocity[i][j].y) * dx;

				//east neighbour
				matrix[J][J] += std::max(0.0, velocity[i][j].x) * dy;
				if (Case[i][j+1] != 0)
					bvec[J] -= Case[i][j+1] * std::min(0.0, velocity[i][j].x) * dy;
				else
					matrix[J][J+1] += std::min(0.0, velocity[i][j].x) * dy;

				//west neighbour
				matrix[J][J] -= std::min (0.0, velocity[i][j].x) * dy;
				if (Case[i][j-1] != 0)
					bvec[J] += Case[i][j-1] * std::max (0.0, velocity[i][j].x) * dy;
				else
					matrix[J][J-1] -= std::max (0.0, velocity[i][j].x) * dy;

				//Cell itself
				if (Case[i][j] != 0){
					for (int k=0; k<matrix[J].size(); k++){
						matrix[J][k] = 0;
					}
					matrix[J][J] = 1;
					bvec[J] = Case[i][j];
				}

				//Wall Boundary for this Case
				if (j > (Case[i].size()/3) && j < (Case[i].size()*2/3) && i == (Case.size()/2-1)){
					//lower airfoil
					//north neighbour == 0
					matrix[J][J] -= std::max(0.0, velocity[i][j].y) * dx;
					bvec[J] += Casemindt[i][j] * std::min(0.0, velocity[i][j].y) * dx;
				}

				else if (j > (Case[i].size()/3) && j < (Case[i].size()*2/3) && i == (Case.size()/2)){
					//upper airfoil
					//south neighbour == 0
					matrix[J][J] += std::min(0.0, velocity[i][j].y) * dx;
					bvec[J] += Casemindt[i][j] * std::max(0.0, velocity[i][j].y) * dx;
				}
			}
			temp = TDMA(matrix, bvec);
			for (int j=0; j<Case.size()-2; j++){
				UpdatedCase[i][j+1] = temp[j];
			}

			for (int i=0; i<matrix.size(); i++){
				bvec[i] = 0;
				for (int j=0; j<matrix[i].size(); j++){
					matrix[i][j] = 0;
				}
			}
		}

		for (int i=0; i<OldCase.size(); i++){
			for (int j=0; j<OldCase[i].size(); j++){
				double cek = std::abs(UpdatedCase[i][j] - OldCase[i][j]);
				error = std::max(error, cek);
				OldCase[i][j] = UpdatedCase[i][j];
			}
		}
		cout<<"Error perhitungan adalah : "<<error<<endl;
	}
	while(error > 0.0001);

	/*
	Deleting unecessary vector
	*/
	matrix.erase(matrix.begin(), matrix.begin()+matrix.size());
	bvec.erase(bvec.begin(), bvec.begin()+bvec.size());
	temp.erase(temp.begin(), temp.begin()+temp.size());
	OldCase.erase(OldCase.begin(), OldCase.begin()+OldCase.size());

	return UpdatedCase;
}